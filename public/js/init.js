var socket = io();
socket.on('addCard', function(data) {
  if(Object.keys(data).length > 0) {
    var content = '<div class="col s12 m6"><div class="card"><div class="card-image">';
    if(data.card.image != "") {
      content += '<img src="' + data.card.image + '" />';
    }

    if(data.card.video != "") {
      content += data.card.video;
    }
    content += '<span class="card-title">' + data.card.title + '</span></div>';
    content += '<div class="card-content"><p>' + data.card.description + '</p></div></div></div>';

    $("#edukia").prepend(content);
  }
});
