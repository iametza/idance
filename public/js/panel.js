var socket;
(function($){
  socket = io();
  $(function(){
    $("body").on("click", ".informazioa", function(ev) {
      ev.preventDefault();
      var id = $(this).data("id");

      socket.emit('informazioa', { card: cards[id] });
      //$("#" + id).remove();
      $("#" + id + " div").addClass("teal lighten-5")
    });
  }); // end of document ready
})(jQuery); // end of jQuery name space
