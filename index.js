var app = require('express')();
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var i = 0;
var sockets = {};
var data = require('./data/data.json');
// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', function(req, res) {
  res.render('pages/index');
});
app.get('/panel', function(req, res) {
  res.render('pages/panel', {
    data: data
  });
});
io.on('connection', function(socket) {
  sockets[socket.id] = socket;
  if (i > 0) {
    for (var j = 1; j <= i; j++) {
      sockets[socket.id].emit('addCard', {
        card: data[j]
      });
    }
  }
  socket.on('disconnect', function() {
    delete sockets[socket.id];
    // no more sockets, kill the stream
    if (Object.keys(sockets).length == 0) {
      i = 0;
    }
  });
  socket.on('informazioa', function(data) {
    i++;
    socket.broadcast.emit('addCard', {
      card: data.card
    });
  });
});
http.listen(3000, function() {
  console.log('listening on *:3000');
});
