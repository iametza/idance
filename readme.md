# iDANCE

## Descripción del proyecto

Con este proyecto se pretende poner en marcha un servidor con el cual se mandarán tarjetas de información en tiempo real a todos los clientes.

## Instalación

Hay que instalar node y npm. Una vez que los hayamos instalado ejecutaremos el siguiente comando

`$ npm install`

Para poner el servidor en marcha

`$ node index.js`

El panel de control está en la siguiente dirección: http://localhost:3000/panel

El usuario entra en está dirección: http://localhost:3000



## Desarrollo

Desarrollado por iametza interaktiboa [garapena@iametza.eus](mailto:garapena@iametza.eus).
